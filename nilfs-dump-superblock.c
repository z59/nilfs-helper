// =nilfs-tune -l

#define _LARGEFILE64_SOURCE
#include <fcntl.h> // open
#include <unistd.h> // lseek64
#include <sys/ioctl.h> // ioctl
#include <stdint.h> // uint64_t
#include <linux/fs.h> // BLKGETSIZE64
#include <stdlib.h> // exit, EXIT_FAILURE
#include <stdio.h> // perror, printf
#include "nilfs2_ondisk.h"

#define STR2(x) #x
#define STR(x) STR2(x)


static void _die(const char *msg) {
	perror(msg);
	exit(EXIT_FAILURE);
}
#define die(msg) _die(__FILE__ ":" STR(__LINE__) ": " msg)

void main(int argc, char *argv[]) {
	if (argc < 2)
		die("Arguments: <device path> [superblock number (0 or 1)]");
	int fd = open(argv[1], O_RDONLY);
	if (fd == -1)
		die("Can't open nilfs2 device");
	off64_t offset = NILFS_SB_OFFSET_BYTES;
	if (argc >= 3 && atoi(argv[2]) == 1) {
		uint64_t size;
		if (ioctl(fd, BLKGETSIZE64, &size) == -1)
			die("Can't get nilfs2 device size");
		offset = NILFS_SB2_OFFSET_BYTES(size);
	}
	if (lseek64(fd, offset, SEEK_SET) == -1)
		die("Can't lseek64");
	struct nilfs_super_block sb;
	if (read(fd, &sb, sizeof(sb)) <= 0)
		die("Read error");

	printf("s_rev_level: %u (Revision level)\n", sb.s_rev_level);
	printf("s_minor_rev_level: %u (minor revision level)\n", sb.s_minor_rev_level);
	printf("s_magic: 0x%04X (Magic signature)\n", sb.s_magic);
	printf("s_bytes: %u (Bytes count of CRC calculation for this structure. s_reserved is excluded.)\n", sb.s_bytes);
	printf("s_flags: %u (flags)\n", sb.s_flags);
	printf("s_crc_seed: %u (Seed value of CRC calculation)\n", sb.s_crc_seed);
	printf("s_sum: %u (Check sum of super block)\n", sb.s_sum);
	printf("s_log_block_size: %u (Block size represented as follows blocksize = 1 << (s_log_block_size + 10))\n", sb.s_log_block_size);
	printf("s_nsegments: %llu (Number of segments in filesystem)\n", sb.s_nsegments);
	printf("s_dev_size: %llu (block device size in bytes)\n", sb.s_dev_size);
	printf("s_first_data_block: %llu (1st seg disk block number)\n", sb.s_first_data_block);
	printf("s_blocks_per_segment: %u (number of blocks per full segment)\n", sb.s_blocks_per_segment);
	printf("s_r_segments_percentage: %u (Reserved segments percentage)\n", sb.s_r_segments_percentage);
	printf("s_last_cno: %llu (Last checkpoint number)\n", sb.s_last_cno);
	printf("s_last_pseg: %llu (disk block addr pseg written last)\n", sb.s_last_pseg);
	printf("s_last_seq: %llu (seq. number of seg written last)\n", sb.s_last_seq);
	printf("s_free_blocks_count: %llu (Free blocks count)\n", sb.s_free_blocks_count);
	printf("s_ctime: %llu (Creation time (execution time of newfs))\n", sb.s_ctime);
	printf("s_mtime: %llu (Mount time)\n", sb.s_mtime);
	printf("s_wtime: %llu (Write time)\n", sb.s_wtime);
	printf("s_mnt_count: %u (Mount count)\n", sb.s_mnt_count);
	printf("s_max_mnt_count: %u (Maximal mount count)\n", sb.s_max_mnt_count);
	printf("s_state: %u (File system state)\n", sb.s_state);
	printf("s_errors: %u (Behaviour when detecting errors)\n", sb.s_errors);
	printf("s_lastcheck: %llu (time of last check)\n", sb.s_lastcheck);
	printf("s_checkinterval: %u (max. time between checks)\n", sb.s_checkinterval);
	printf("s_creator_os: %u (OS)\n", sb.s_creator_os);
	printf("s_def_resuid: %u (Default uid for reserved blocks)\n", sb.s_def_resuid);
	printf("s_def_resgid: %u (Default gid for reserved blocks)\n", sb.s_def_resgid);
	printf("s_first_ino: %u (First non-reserved inode)\n", sb.s_first_ino);
	printf("s_inode_size: %u (Size of an inode)\n", sb.s_inode_size);
	printf("s_dat_entry_size: %u (Size of a dat entry)\n", sb.s_dat_entry_size);
	printf("s_checkpoint_size: %u (Size of a checkpoint)\n", sb.s_checkpoint_size);
	printf("s_segment_usage_size: %u (Size of a segment usage)\n", sb.s_segment_usage_size);
	printf("s_uuid: 0x%016lX%016lX (128-bit uuid for volume)\n", *(uint64_t*)(sb.s_uuid+8), *(uint64_t*)sb.s_uuid);
	printf("s_volume_name: \"%s\" (volume name)\n", sb.s_volume_name);
	printf("s_c_interval: %u (Commit interval of segment)\n", sb.s_c_interval);
	printf("s_c_block_max: %u (Threshold of data amount for the segment construction)\n", sb.s_c_block_max);
	printf("s_feature_compat: %llu (Compatible feature set)\n", sb.s_feature_compat);
	printf("s_feature_compat_ro: %llu (Read-only compatible feature set)\n", sb.s_feature_compat_ro);
	printf("s_feature_incompat: %llu (Incompatible feature set)\n", sb.s_feature_incompat);
}
