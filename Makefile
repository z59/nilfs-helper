CFLAGS ?= -O2 -static
CFLAGS += -Iinclude

.PHONY: all clean
all: $(patsubst %.c,%,$(wildcard *.c))
clean:
	git clean -dfX
