#define _LARGEFILE64_SOURCE
#include <fcntl.h> // open
#include <unistd.h> // lseek64
#include <sys/ioctl.h> // ioctl
#include <stdint.h> // uint64_t
#include <linux/fs.h> // BLKGETSIZE64
#include <stdlib.h> // exit, EXIT_SUCCESS, EXIT_FAILURE
#include <stdio.h> // perror, printf
#include "mkfs.h"
#include "crc32.h"

#define STR2(x) #x
#define STR(x) STR2(x)
#define ret(msg) { perror(__FILE__ ":" STR(__LINE__) ": " msg); return EXIT_FAILURE; }


static inline int read_superblock(
	const int fd
	, uint64_t *size
	, struct nilfs_super_block *sb
) {
	if (ioctl(fd, BLKGETSIZE64, size) == -1)
		ret("Can't get nilfs2 device size");
	if (lseek64(fd, NILFS_SB_OFFSET_BYTES, SEEK_SET) != NILFS_SB_OFFSET_BYTES)
		ret("Can't lseek64");
	if (read(fd, sb, sizeof(*sb)) != sizeof(*sb))
		ret("Read error");
	return EXIT_SUCCESS;
}

static inline int find_last_seq(
	const int fd
	, const struct nilfs_super_block *sb
	, uint64_t *last_pseg
	, struct nilfs_segment_summary *ss
) {
	uint32_t log_block_size = le32_to_cpu(sb->s_log_block_size) + 10;
	uint64_t last_pos = 0;
	{
		uint64_t start_pos = (le64_to_cpu(sb->s_first_data_block) << log_block_size) + offsetof(struct nilfs_segment_summary, ss_seq);
		uint64_t seg_size = le32_to_cpu(sb->s_blocks_per_segment) << log_block_size;
		uint64_t end_pos = le64_to_cpu(sb->s_nsegments) * seg_size;
		uint64_t pos = start_pos, last_seq = 0, seq;
		while (pos < end_pos) {
			if (lseek64(fd, pos, SEEK_SET) != pos)
				ret("Can't lseek64");
			if (read(fd, &seq, sizeof(seq)) != sizeof(seq))
				ret("Read error");
			seq = le64_to_cpu(seq);
			if (seq > last_seq) {
				last_seq = seq;
				last_pos = pos;
			}
			if (pos == start_pos)
				pos = pos / seg_size + seg_size + offsetof(struct nilfs_segment_summary, ss_seq);
			else
				pos += seg_size;
		}
	}
	last_pos -= offsetof(struct nilfs_segment_summary, ss_seq);
	if (lseek64(fd, last_pos, SEEK_SET) != last_pos)
		ret("Can't lseek64");
	if (read(fd, ss, sizeof(*ss)) != sizeof(*ss))
		ret("Read error");
	*last_pseg = last_pos >> log_block_size;
	return EXIT_SUCCESS;
}

static inline int write_superblocks(
	const int fd
	, const uint64_t size
	, const uint64_t last_pseg
	, const struct nilfs_segment_summary *ss
	, struct nilfs_super_block *sb
) {
	sb->s_last_cno = ss->ss_cno;
	sb->s_last_pseg = cpu_to_le64(last_pseg + le32_to_cpu(ss->ss_nblocks) );
	sb->s_last_seq = ss->ss_seq;
	sb->s_mtime = sb->s_ctime;
	sb->s_wtime = ss->ss_create;
	sb->s_mnt_count = cpu_to_le64(1);
	//sb->s_state = 0;
	sb->s_sum = 0;
	sb->s_sum = cpu_to_le32(crc32_le(le32_to_cpu(sb->s_crc_seed), (char*)&sb, NILFS_SB_BYTES));
	if (lseek64(fd, NILFS_SB_OFFSET_BYTES, SEEK_SET) != NILFS_SB_OFFSET_BYTES)
		ret("Can't lseek64");
	if (write(fd, sb, sizeof(*sb)) != sizeof(*sb))
		ret("write failed");
	if (lseek64(fd, NILFS_SB2_OFFSET_BYTES(size), SEEK_SET) != NILFS_SB2_OFFSET_BYTES(size))
		ret("Can't lseek64");
	if (write(fd, sb, sizeof(*sb)) != sizeof(*sb))
		ret("write failed");
	return EXIT_SUCCESS;
}

int main(const int argc, const char *argv[]) {
	if (argc < 2)
		ret("Arguments: <device path>");
	int fd = open(argv[1], O_RDWR);
	if (fd == -1)
		ret("Can't open nilfs2 device");
	uint64_t size;
	struct nilfs_super_block sb;
	if (read_superblock(fd, &size, &sb) != EXIT_SUCCESS)
		ret("read_superblock failed");
	uint64_t last_pseg;
	struct nilfs_segment_summary ss;
	if (find_last_seq(fd, &sb, &last_pseg, &ss) != EXIT_SUCCESS)
		ret("find_last_seq failed");
	if (write_superblocks(fd, size, last_pseg, &ss, &sb) != EXIT_SUCCESS)
		ret("write_superblocks failed");
	return EXIT_SUCCESS;
}
